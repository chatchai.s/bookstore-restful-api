# Getting Started    
- Data Definition in Doc Folder
    - database design
    - sequence diagrams
    - API documentation (Swagger) in `http://localhost:8090/bookapi/swagger-ui.html#/`
    - unitest in postman
## Installation
- git clone https://gitlab.com/chatchai.s/bookstore-restful-api.git
- setup docker and launch docker
- run command compose setup database 
```bash
cd docker
docker compose up
```
- setup java jdk and maven
- start project 
```bash
mvn clean compile
mvn spring-boot:run
```
## Usage
    - Docker
    - Java Spring boot
    - maven
    - postman
## Features
    - create user
    - delete user
    - login user
    - get user detail
    - order book
    - list book
## Example
## Tests
## Dependencies
## Thanks
