package bookstore.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import bookstore.handler.BookstoreException;
import bookstore.res.BookDataPub;
import bookstore.util.ErrorCode;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PubService {

	public BookDataPub[] getBookFromPub() {
		String url = "https://scb-test-book-publisher.herokuapp.com/books";
		try {
			RestTemplate restTemplate = new RestTemplate();
			log.info("Request " + url);
			ResponseEntity<BookDataPub[]> response = restTemplate.getForEntity(url, BookDataPub[].class);
			return response.getBody();
		} catch (Exception e) {
			throw new BookstoreException("Fail request " + url, ErrorCode.WHEN_CALL_API, e);
		}
	}

	public BookDataPub[] callGetListRecommendedFromPublisher() {
		log.debug("callGetListRecommendedFromPublisher");
		String url = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";
		try {
			RestTemplate restTemplate = new RestTemplate();
			log.info("Request " + url);
			ResponseEntity<BookDataPub[]> response = restTemplate.getForEntity(url, BookDataPub[].class);
			return response.getBody();
		} catch (Exception e) {
			throw new BookstoreException("Fail request " + url, ErrorCode.WHEN_CALL_API, e);
		}
	}

}
