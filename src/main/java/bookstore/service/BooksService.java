package bookstore.service;

import bookstore.entity.BookEntity;
import bookstore.inter.BookRepository;
import bookstore.res.BooksRespon;
import bookstore.res.BookDataPub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BooksService {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	PubService publisherService;

	public List<BooksRespon> getBooks() {

		List<BooksRespon> dataList = new ArrayList<>();
		BookDataPub[] recommendList = publisherService.callGetListRecommendedFromPublisher();
		List<Long> recommendIdList = new ArrayList<>();
		for (BookDataPub book : recommendList) {
			BooksRespon data = new BooksRespon();
			recommendIdList.add(book.getId());
			data.setId(book.getId());
			data.setName(book.getBook_name());
			data.setAuthor(book.getAuthor_name());
			data.setPrice(book.getPrice());
			data.setIs_recommended(true);
			dataList.add(data);
		}

		List<BookEntity> bookList = bookRepository.findByIdNotIn(recommendIdList);

		for (BookEntity book : bookList) {
			BooksRespon data = new BooksRespon();
			data.setId(book.getId());
			data.setName(book.getName());
			data.setAuthor(book.getAuthor());
			data.setPrice(book.getPrice());
			data.setIs_recommended(false);
			dataList.add(data);
		}

		return dataList;
	}

	public List<BookEntity> update(List<BookEntity> bookEntityList) {
		List<BookEntity> bookList = bookRepository.saveAll(bookEntityList);
		return bookList;
	}
}
