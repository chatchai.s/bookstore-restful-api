package bookstore.handler;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import bookstore.util.ErrorCode;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	protected ResponseEntity<Object> handleException(Exception ex) {
		return buildResponseEntity(new ApiError(ErrorCode.UNKNOWN_ERROR, "unknown error.", ex));
	}

	@ExceptionHandler(BookstoreException.class)
	protected ResponseEntity<Object> handleBookstoreException(BookstoreException ex) {
		return buildResponseEntity(new ApiError(ex.getErrCode(), ex.getMessage(), ex));
	}

	private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
		return new ResponseEntity<>(apiError, HttpStatus.SERVICE_UNAVAILABLE);
	}

}