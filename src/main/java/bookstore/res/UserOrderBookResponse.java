package bookstore.res;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class UserOrderBookResponse {
	private BigDecimal price;
}
