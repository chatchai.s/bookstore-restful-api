package bookstore.res;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BookDataPub {

	private Long id;
	private String book_name;
	private String author_name;
	private BigDecimal price;

}
