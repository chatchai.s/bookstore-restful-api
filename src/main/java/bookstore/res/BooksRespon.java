package bookstore.res;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BooksRespon {

	private Long id;
	private String name;
	private String author;
	private BigDecimal price;
	private Boolean is_recommended;

}
