package bookstore.res;

import lombok.Data;

import java.util.List;

@Data
public class UserDataResponse {
	private String name;
	private String surname;
	private String date_of_birth;
	private List<Long> books;
}
