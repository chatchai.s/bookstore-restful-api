package bookstore.util;

public class ErrorCode {
	public static final String UNKNOWN_ERROR = "500";
	public static final String NO_DATA_FOUND = "403";
	public static final String CANNOT_SAVE_DATA = "400";
	public static final String CANNOT_LOAD_DATA = "502";
	public static final String DUPLICATE_DATA = "404";
	public static final String UPDATE_BOOK_FAIL = "501";
	public static final String WHEN_CALL_API = "503";
}
