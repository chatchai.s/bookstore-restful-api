package bookstore.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	public static String formatDate(Date date, String pattern) {
		return formatDate(date, pattern, Locale.US);
	}

	public static String formatDate(Date date, String pattern, Locale locale) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat(pattern, locale);
		return df.format(date);
	}

	public static Date parseDate(String dateText, String pattern) {
		return parseDate(dateText, pattern, Locale.US);
	}

	public static Date parseDate(String dateText, String pattern, Locale locale) {
		if (dateText == null || dateText.isEmpty())
			return null;
		try {
			SimpleDateFormat df = new SimpleDateFormat(pattern, locale);
			return df.parse(dateText);
		} catch (ParseException e) {
			return null;
		}
	}

}
