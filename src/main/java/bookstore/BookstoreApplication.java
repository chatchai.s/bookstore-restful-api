package bookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import bookstore.job.Job;

@SpringBootApplication
@EnableScheduling
public class BookstoreApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(BookstoreApplication.class, args);
		
//		updated on a weekly basic (every Sunday at midnight).
		Job job = applicationContext.getBean(Job.class);
		job.loadBook();
	}

}
