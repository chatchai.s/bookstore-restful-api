package bookstore.req;

import java.util.List;

import lombok.Data;

@Data
public class UserOrderBookRequest {
	private List<Long> orders;
}
