package bookstore.req;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class UserDataRequest {
	@NotEmpty
	@Size(max = 100)
	private String username;

	@NotEmpty
	private String password;

	@Size(max = 250)
	private String name;

	@Size(max = 250)
	private String surname;

	private Date date_of_birth;
}
