package bookstore.inter;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bookstore.entity.OrderEntity;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {

	public List<OrderEntity> findIdByUserId(Long userId);

	@Query("SELECT " 
			+ "   orderDetail.bookId " 
			+ " FROM OrderEntity orderHeader "
			+ "   JOIN OrderDetailEntity orderDetail ON orderDetail.orderId = orderHeader.id "
			+ " WHERE orderHeader.userId = :userId " + " GROUP BY orderDetail.bookId ")
	public List<Long> findBookIdByUserId(@Param("userId") Long userId);
}
