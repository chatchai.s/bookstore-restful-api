package bookstore.job;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import bookstore.entity.BookEntity;
import bookstore.handler.BookstoreException;
import bookstore.res.BookDataPub;
import bookstore.service.BooksService;
import bookstore.service.PubService;
import bookstore.util.ErrorCode;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Job {

	@Autowired
	PubService pubService;

	@Autowired
	BooksService booksService;

	// (every Sunday at midnight).
	// [s m h D M DAY]? = skip
	@Scheduled(cron = "0 1 0 ? * SUN")
	public void loadBook() {
		BookDataPub[] data = pubService.getBookFromPub();

		List<BookEntity> listBook = new ArrayList<>();
		for (BookDataPub bookData : data) {
			BookEntity book = new BookEntity();
			book.setId(bookData.getId());
			book.setName(bookData.getBook_name());
			book.setAuthor(bookData.getAuthor_name());
			book.setPrice(bookData.getPrice());
			listBook.add(book);
		}

		List<BookEntity> listUpdate = booksService.update(listBook);

		if (listUpdate.equals(listBook)) {
			log.info("Update success");
		} else {
			log.error("Update fail");
			throw new BookstoreException("Update list of book fail", ErrorCode.UPDATE_BOOK_FAIL);
		}
	}

}
