CREATE SCHEMA dev AUTHORIZATION postgres ;

CREATE TABLE dev.book (
	id int8 NOT NULL,
	"name" varchar(1000) NULL,
	author varchar(1000) NULL,
	price numeric(12,2) NOT NULL,
	CONSTRAINT book_pkey PRIMARY KEY (id)
);

CREATE TABLE dev.user_bookstore (
	id int8 NOT NULL,
	username varchar(100) NOT NULL,
	"password" varchar(500) NOT NULL,
	date_of_birth date NULL,
	"name" varchar(250) NULL,
	surname varchar(250) NULL,
	CONSTRAINT user_bookstore_pk PRIMARY KEY (id),
	CONSTRAINT user_bookstore_un UNIQUE (username)
);


CREATE TABLE dev.order_bookstore (
	id int8 NOT NULL,
	user_id int8 NOT NULL,
	price numeric(12,2) NULL,
	CONSTRAINT order_bookstore_pk PRIMARY KEY (id),
	CONSTRAINT order_bookstore_user_bookstore_fk FOREIGN KEY (user_id) REFERENCES dev.user_bookstore(id)
);

CREATE TABLE dev.order_detail (
	id int8 NOT NULL,
	order_id int8 NULL,
	book_id int8 NOT NULL,
	CONSTRAINT order_detail_pk PRIMARY KEY (id),
	CONSTRAINT order_detail_order_bookstore_fk FOREIGN KEY (order_id) REFERENCES dev.order_bookstore(id)
);

CREATE SEQUENCE dev.user_bookstore_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1;

CREATE SEQUENCE dev.order_bookstore_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1;

CREATE SEQUENCE dev.order_detail_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1;
